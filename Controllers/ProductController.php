<?php

namespace Controllers;

require_once '/Users/Chucky/Desktop/scandiweb-bitbucket/Database/MySqlConnection.php';
require_once '/Users/Chucky/Desktop/scandiweb-bitbucket/Classes/Product.php';
require_once '/Users/Chucky/Desktop/scandiweb-bitbucket/Classes/Book.php';
require_once '/Users/Chucky/Desktop/scandiweb-bitbucket/Classes/Dvd.php';
require_once '/Users/Chucky/Desktop/scandiweb-bitbucket/Classes/Furniture.php';


use Classes\Product;
use Classes\Book;
use Classes\Dvd;
use Classes\Furniture;

class ProductController
{
    public static function createProduct(array $info, $pdo)
    {
        $productType = ucwords($info['productType']);
        $className = "Classes\\".$productType;
        $product = new $className($info);
        
        if($product){
            return  $product->storeInDB($pdo);
        }else{
            return false;
        }
    }
    public static function listProducts($pdo)
    {
        $dvds = Dvd::fetchFromDb($pdo);
        $books = Book::fetchFromDb($pdo);
        $furnitures = Furniture::fetchFromDb($pdo);
        return compact('dvds', 'books', 'furnitures');
    }
    public static function deleteProduct($pdo, $products)
    {
        foreach ($products as $product) {
            foreach ($product as $productData) {
                $data = explode('-', $productData);
                $table = $data[0];
                $id = intval($data[1]);
                $stmt = $pdo->prepare("DELETE FROM $table WHERE id=$id");
                $stmt->execute();
            }
        }
    }
}
