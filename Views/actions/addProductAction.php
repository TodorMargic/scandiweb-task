<?php
require_once '/Users/Chucky/Desktop/scandiweb-bitbucket/Database/MySqlConnection.php';
require_once '/Users/Chucky/Desktop/scandiweb-bitbucket/Controllers/ProductController.php';


use Controllers\ProductController;
use Database\MysqlConnection;



if ($_SERVER["REQUEST_METHOD"] === "POST") {
    
    if (ProductController::createProduct($_POST, $pdo)) {
        header("Location: ../listProducts.php");
    } else {
        $message = 'Please fill all the requirements';
        header("Location: ../addProduct.php?message=".$message);
    }
}
