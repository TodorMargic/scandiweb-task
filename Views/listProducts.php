<?php

include_once 'partials/header.view.php';
require_once '/Users/Chucky/Desktop/scandiweb-bitbucket/Database/MySqlConnection.php';
require_once '/Users/Chucky/Desktop/scandiweb-bitbucket/Controllers/ProductController.php';

use Controllers\ProductController;

$data = ProductController::listProducts($pdo);

?>

<div class="container p-2 ">
    <header class="row mx-0 ">
        <div class="col-md-6 ">
            <h2>Product list</h2>
        </div>
        <div class="col-md-2 offset-md-1">
            <a class="btn btn-success col-md-10" href="addProduct.php">ADD</a>
        </div>
        <div class="col-md-2">
            <button class=" btn btn-danger col-md-10" id='deleteProductBtn'>MASS DELETE</button>
            <div>
    </header>

    <form action="../Views/actions/massDeleteAction.php" method="POST" id="productList" class="row mx-0 p-3 border-top border-bottom ">
        <?php
        foreach ($data['books'] as $book) {
            echo $book->printCard($book);
        }
        foreach ($data['dvds'] as $dvd) {
            echo $dvd->printCard($dvd);
        }
        foreach ($data['furnitures'] as $furniture) {
            echo $furniture->printCard($furniture);
        }
        ?>
    </form>

    <footer class="row mx-0 text-center p-3">
        <h4 class='col-md-12'>Scandiweb Test assignment</h4>
    </footer>
</div>


<?php

include_once "partials/footer.view.php";

?>