<?php
include_once "partials/header.view.php";
?>
<div class="container-fluid p-3">
    <div class="row mx-0 border-bottom p-3">
        <div class="col-md-6 offset-md-2">
            <h3>Add Product</h3>
        </div>
        <div class="offset-md-1 col-md-1 ">
            <button class="btn btn-primary col-md-10" id="submitForm">Save</button>
        </div>
        <div class="col-md-1">
            <a class="btn btn-warning col-md-10" href="listProducts.php">Cancel</a>
        </div>
    </div>
    <div class="row mx-0 py-3">
        <div class="col-md-4 offset-md-4 pb-3">
            <?php if (isset($_GET['message'])) {
                echo "<p id='feedbackParagraph' class='p-0 m-0 alert alert-danger   text-center'>{$_GET['message']}</p>";
            } ?>
        </div>

        <form action="/Views/actions/addProductAction.php" method="POST" id="product_form" class="offset-md-1 col-md-10">
            <div class="form-group row mx-0">
                <label for="sku" class="offset-md-3 col-md-3 py-2">SKU</label>
                <input type="text" class="form-control col-md-3" id="sku" placeholder="Enter SKU" name="sku">
                <small class="offset-md-6 col-md-3 text-primary text-center" id="skuDesc">Type the SKU of the product</small>
            </div>
            <div class="form-group row mx-0">
                <label for="name" class="offset-md-3 col-md-3 py-2">Name</label>
                <input type="text" class="form-control col-md-3" id="name" placeholder="Enter Name" name="name">
                <small class="offset-md-6 col-md-3 text-primary text-center" id="nameDesc">Type the name of the product</small>
            </div>
            <div class="form-group row mx-0">
                <label for="price" class=" offset-md-3 col-md-3 py-2">Price (&#36;)</label>
                <input type="number" class="form-control col-md-3" id="price" name="price" placeholder="Enter price in dollars">
                <small class="offset-md-6 col-md-3 text-primary text-center" id="priceDesc">The value needs to be in dollars (&#36;) </small>
            </div>
            <div class="form-group row mx-0">
                <label for="productType" class=" offset-md-3 col-md-3 py-2">Type Switcher</label>
                <select class="form-control col-md-3" id="productType" name="productType">
                    <option selected>Choose type</option>
                    <option value="Dvd">DVD</option>
                    <option value="Book">Book</option>
                    <option value="Furniture">Furniture</option>
                </select>
                <small class="offset-md-6 col-md-3 mb-2 text-primary text-center" id="selectDesc">Please select product type!</small>
            </div>
            <div class="form-group row mx-0 dvd d-none">
                <label for="dvd" class=" offset-md-3 col-md-3 py-2">Size</label>
                <input type="number" class="form-control col-md-3" name="size" id="size">
                <small class="offset-md-6 col-md-3 text-primary text-center" id="sizeDesc">The value needs to be in MB</small>
            </div>
            <div class="form-group row mx-0 book d-none">
                <label for="book" class=" offset-md-3 col-md-3 py-2">Weight</label>
                <input type="number" class="form-control col-md-3" name="weight" id="weight">
                <small class="offset-md-6 col-md-3 text-primary text-center" id="weightDesc">The value needs to be in Kg</small>
            </div>
            <div class="form-group row mx-0 furniture d-none">
                <label for="furnitureWidth" class=" offset-md-3 col-md-3 py-2">Width</label>
                <input type="number" class="form-control col-md-3" name="width" id="width">
                <small class="offset-md-6 col-md-3 mb-2 text-primary text-center" id="widthDesc">The value needs to be in CM</small>
                <label for="furnitureHeight" class=" offset-md-3 col-md-3 py-2">Height</label>
                <input type="number" class="form-control col-md-3" name="height" id="height">
                <small class="offset-md-6 col-md-3 mb-2 text-primary text-center" id="heightDesc">The value needs to be in CM</small>
                <label for="furnitureLength" class=" offset-md-3 col-md-3 py-2">Length</label>
                <input type="number" class="form-control col-md-3" name="length" id="length">
                <small class="offset-md-6 col-md-3 mb-2 text-primary text-center" id="lengthDesc">The value needs to be in CM</small>
            </div>
        </form>
    </div>
</div>

<?php
include_once "partials/footer.view.php";
?>