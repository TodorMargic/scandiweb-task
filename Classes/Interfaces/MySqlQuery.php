<?php
namespace Interfaces;

interface MySqlQuery
{
    public static function fetchFromDb($pdo);
    
    public function storeInDb($pdo);
}
