<?php

namespace Classes;

Abstract class Product
{
    public $sku;
    public $name;
    public $price;
   

    public function __construct(string $sku,string $name, int $price)
    {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
    }

    abstract function printCard($object);
    abstract function storeInDb($pdo);
    
}