<?php

namespace Classes;

require_once "/Users/Chucky/Desktop/scandiweb-bitbucket/Classes/Product.php";
require_once "/Users/Chucky/Desktop/scandiweb-bitbucket/Classes/Interfaces/MySqlQuery.php";

use Classes\Product;
use Interfaces\MySqlQuery;

class Dvd extends Product implements MySqlQuery
{
    public $size;

    public function __construct($info)
    {
        if (
            isset($info['size']) && preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $info['size']) && isset($info['sku']) && preg_match('/[a-zA-Z0-9]/', $info['sku'])
            && isset($info['name']) && preg_match('/[a-zA-Z]/', $info['name']) && isset($info['price']) && preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $info['price'])

        ) {
            $this->size = floatval($info['size']);
            parent::__construct($info['sku'], $info['name'], intval($info['price']));
        }
    }

    public function printCard($object)
    {
        $card = "<div class='col-md-3 p-2'>
                    <div class='border p-2'>
                        <input type='checkbox' value='dvds-{$object->id}' class='delete-checkbox' name='products[]'>
                        <div class='text-center font-weight-bold'>
                            <p>{$object->sku}</p>
                            <p>{$object->name}</p>
                            <p>{$object->price}&#36;</p>
                            <p>Size: {$object->size}MB</p>
                        </div>
                    </div>
                </div>";
        return $card;
    }

    public function storeInDB($pdo)
    {
        $query = " INSERT INTO dvds (sku, name, price, size)
        VALUES (:sku, :name, :price, :size);";
        $stmt = $pdo->prepare($query);
        return $stmt->execute(['sku' => $this->sku, 'name' => $this->name, 'price' => intval($this->price), 'size' => floatval($this->size)]);
    }
    public static function fetchFromDb($pdo)
    {
        $dvd = $pdo->query('SELECT * FROM dvds')
            ->fetchAll(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, 'Classes\Dvd', [floatval('size'), 'sku', 'name', intval('price')]);
        return $dvd;
    }
}
