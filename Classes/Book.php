<?php

namespace Classes;

require_once "/Users/Chucky/Desktop/scandiweb-bitbucket/Classes/Product.php";
require_once "/Users/Chucky/Desktop/scandiweb-bitbucket/Classes/Interfaces/MySqlQuery.php";

use Classes\Product;
use Interfaces\MySqlQuery;

class Book extends Product implements MySqlQuery
{
    public $weight;

    public function __construct($info)
    {
        if (
            isset($info['weight']) && preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $info['weight']) && isset($info['sku']) && preg_match('/[a-zA-Z0-9]/', $info['sku'])
            && isset($info['name']) && preg_match('/[a-zA-Z]/', $info['name']) && isset($info['price']) && preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $info['price'])
        ) {
            $this->weight = floatval($info['weight']);
            parent::__construct($info['sku'], $info['name'], intval($info['price']));
        }
    }

    public function printCard($object)
    {
        $card = "<div class='col-md-3 p-2'>
                    <div class='border p-2'>
                        <input type='checkbox' value='books-{$object->id}' class='delete-checkbox' name='products[]'>
                        <div class='text-center font-weight-bold'>
                            <p>{$object->sku}</p>
                            <p>{$object->name}</p>
                            <p>{$object->price}&#36;</p>
                            <p>Weight: {$object->weight}KG</p>
                        </div>
                    </div>
                </div>";
        return $card;
    }
    public function storeInDB($pdo)
    {
        $query = " INSERT INTO books (sku, name, price, weight)
        VALUES (:sku, :name, :price, :weight);";
        $stmt = $pdo->prepare($query);
        return $stmt->execute(['sku' => $this->sku, 'name' => $this->name, 'price' => $this->price, 'weight' => $this->weight]);
    }
    public static function fetchFromDb($pdo)
    {
        $books = $pdo->query('SELECT * FROM books')
            ->fetchAll(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, 'Classes\Book', [floatval('weight'), 'sku', 'name', intval('price')]);

        return $books;
    }
}
