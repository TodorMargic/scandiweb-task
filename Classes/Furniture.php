<?php

namespace Classes;

require_once "/Users/Chucky/Desktop/scandiweb-bitbucket/Classes/Product.php";
require_once "/Users/Chucky/Desktop/scandiweb-bitbucket/Classes/Interfaces/MySqlQuery.php";

use Classes\Product;
use Interfaces\MySqlQuery;


class Furniture extends Product implements MySqlQuery
{
    public $height;
    public $width;
    public $length;

    public function __construct($info)
    {
        if (
            isset($info['height']) && preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $info['height']) && isset($info['length']) && preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $info['length'])
            && isset($info['width']) && preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $info['width']) && isset($info['sku']) && preg_match('/[a-zA-Z0-9]/', $info['sku']) && isset($info['name']) && preg_match('/[a-zA-Z]/', $info['name'])
            && isset($info['price']) && preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $info['price'])
        ) {
            $this->height = floatval($info['height']);
            $this->width = floatval($info['width']);
            $this->length = floatval($info['length']);
            parent::__construct($info['sku'], $info['name'], intval($info['price']));
        }
    }

    public function printCard($object)
    {
        $card = "<div class='col-md-3 p-2'>
                    <div class='border p-2'>
                        <input type='checkbox' class='delete-checkbox' value='furnitures-{$object->id}' name='products[]'>
                        <div class='text-center font-weight-bold'>
                            <p>{$object->sku}</p>
                            <p>{$object->name}</p>
                            <p>{$object->price}&#36;</p>
                            <p>Dimensions: {$object->height}x{$object->width}x{$object->length}</p>
                        </div>
                    </div>
                </div>";
        return $card;
    }
    public function storeInDB($pdo)
    {
        $query = " INSERT INTO furnitures (sku, name, price, height, width, length)
        VALUES (:sku, :name, :price, :height, :width, :length);";
        $stmt = $pdo->prepare($query);
        return  $stmt->execute(['sku' => $this->sku, 'name' => $this->name, 'price' => $this->price, 'height' => $this->height, 'width' => $this->width, 'length' => $this->length]);
    }
    public static function fetchFromDb($pdo)
    {
        $furnitures = $pdo->query('SELECT * FROM furnitures')
            ->fetchAll(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, 'Classes\Furniture', [floatval('height'), floatval('width'), floatval('length'), 'sku', 'name', intval('price')]);
        return $furnitures;
    }
}
