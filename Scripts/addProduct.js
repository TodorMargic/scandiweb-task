$(document).ready(function () {
    const nameRegex = new RegExp(/^[a-zA-Z\s]+$/);
    const skuRegex = new RegExp(/[a-zA-Z0-9]/);
    const floatRegex = new RegExp(/^[0-9]+(\.[0-9]{1,2})?$/);
    $('#productType').on('change', function (e) {
        let optionValue = $('#productType').children("option:selected").val();
        switch (optionValue) {
            case 'Dvd':
                hideElements();
                $('.dvd').removeClass("d-none");
                break;
            case 'Book':
                hideElements();
                $('.book').removeClass("d-none");
                break;
            case 'Furniture':
                hideElements();
                $('.furniture').removeClass("d-none");
                break;
            default:
                break;
        }
    });
    $('#submitForm').on('click', function (e) {
        let optionValue = $('#productType').children("option:selected").val();
        switch (optionValue) {
            case 'Dvd':
                validateDvdInputs();
                break;
            case 'Book':
                validateBookInputs();
                break;
            case 'Furniture':
                validateFurnitureInputs();
                break;
            default:
                validatePrimaryInputs();
                break;               
        }
    });
    const hideElements = function () {
        $('.dvd').addClass("d-none");
        $('.furniture').addClass("d-none");
        $('.book').addClass("d-none");
    };
    const validatePrimaryInputs = function (e) {

        let isValidatedName = validateNameInput();
        let isValidatedSku = validateSkuInput();
        let isValidatedPrice = validatePriceInput();
        let isValidatedSelect = validateSelect();
       
        if (isValidatedName && isValidatedSku && isValidatedPrice &&isValidatedSelect) {
            return true;
        }
    }
    //Book product input fields validation
    const validateBookInputs = function () {
        let areValidatedPrimaryInputs = validatePrimaryInputs();
        let isValidatedWeight = validateWeightInput();
        if (areValidatedPrimaryInputs && isValidatedWeight) {
            return $('#product_form').submit();
        }
    }
    //Furniture product input fields validation
    const validateFurnitureInputs = function () {
        let areValidatedPrimaryInputs = validatePrimaryInputs();
        let isValidatedWidth = validateWidthInput();
        let isValidatedHeight = validateHeightInput();
        let isValidatedLength = validateLenghtInput();
        if (areValidatedPrimaryInputs && isValidatedWidth && isValidatedHeight && isValidatedLength) {
            return $('#product_form').submit();
        }

    }
    //Dvd product input fields validation
    const validateDvdInputs = function () {
        let areValidatedPrimaryInputs = validatePrimaryInputs();
        let isValidatedSize = validateSizeInput();
        if (areValidatedPrimaryInputs && isValidatedSize) {
            return $('#product_form').submit();
        }
    }
    const validateNameInput = function () {
        let nameInput = $("#name");
        let nameInputValue = nameInput.val();
        if (nameInputValue !== '') {
            if (nameRegex.test(nameInputValue)) {
                return true;
            } else {
                nameInput.next().text('The name only can contain letters!').removeClass('text-primary').addClass('text-danger');
            }
        } else {
            nameInput.next().text('Please type the Name').removeClass('text-primary').addClass('text-danger');
        }
    }
    // Validation for every input field separately
    const validateSkuInput = function () {
        let skuInput = $("#sku");
        let skuInputValue = skuInput.val();
        if (skuInputValue !== '') {
            if (skuRegex.test(skuInputValue)) {
                return true;
            } else {
                skuInput.next().text('The SKU only can contain letters and numbers!').removeClass('text-primary').addClass('text-danger');
            }
        } else {
            skuInput.next().text('Please type the SKU').removeClass('text-primary').addClass('text-danger');
        }
    }
    const validatePriceInput = function () {
        let priceInput = $("#price");
        let priceInputValue = priceInput.val();
        if (priceInputValue !== '') {
            if (skuRegex.test(priceInputValue)) {
                return true;
            } else {
                priceInput.next().text('The Price can be round or decimal number!').removeClass('text-primary').addClass('text-danger');
            }
        } else {
            priceInput.next().text('Please type Price, it can be round or decimal number!').removeClass('text-primary').addClass('text-danger');
        }
    }
    const validateSizeInput = function () {
        let sizeInput = $("#size");
        let sizeInputValue = sizeInput.val();
        if (sizeInputValue !== '') {
            if (floatRegex.test(sizeInputValue)) {
                return true;
            } else {
                sizeInput.next().text('The Size can be round or decimal number!').removeClass('text-primary').addClass('text-danger');
            }
        } else {
            sizeInput.next().text('Please type Size, it can be round or decimal number!').removeClass('text-primary').addClass('text-danger');
        }
    }
    const validateWeightInput = function () {
        let weightInput = $("#weight");
        let weightInputValue = weightInput.val();
        if (weightInputValue !== '') {
            if (floatRegex.test(weightInputValue)) {
                return true;
            } else {
                weightInput.next().text('The Weight can be round or decimal number!').removeClass('text-primary').addClass('text-danger');
            }
        } else {
            weightInput.next().text('Please type Weight, it can be round or decimal number!').removeClass('text-primary').addClass('text-danger');
        }
    }
    const validateWidthInput = function () {
        let widthInput = $("#width");
        let widthInputValue = widthInput.val();
        if (widthInputValue !== '') {
            if (floatRegex.test(widthInputValue)) {
                return true;
            } else {
                widthInput.next().text('The Width can be round or decimal number!').removeClass('text-primary').addClass('text-danger');
            }
        } else {
            widthInput.next().text('Please type Width, it can be round or decimal number!').removeClass('text-primary').addClass('text-danger');
        }
    }
    const validateHeightInput = function () {
        let heightInput = $("#height");
        let heightInputValue = heightInput.val();
        if (heightInputValue !== '') {
            if (floatRegex.test(heightInputValue)) {
                return true;
            } else {
                heightInput.next().text('The Height can be round or decimal number!').removeClass('text-primary').addClass('text-danger');
            }
        } else {
            heightInput.next().text('Please type Height, it can be round or decimal number!').removeClass('text-primary').addClass('text-danger');
        }
    }
    const validateLenghtInput = function () {
        let lengthInput = $("#length");
        let lengthInputValue = lengthInput.val();
        if (lengthInputValue !== '') {
            if (floatRegex.test(lengthInputValue)) {
                return true;
            } else {
                lengthInput.next().text('The Length can be round or decimal number!').removeClass('text-primary').addClass('text-danger');
            }
        } else {
            lengthInput.next().text('Please type Length, it can be round or decimal number!').removeClass('text-primary').addClass('text-danger');
        }
    }
    const validateSelect = function(){
        let selectDesc = $('#selectDesc');
        let optionValue = $('#productType').val();
        if(optionValue !== 'Choose type'){
            return true
        }else{
            selectDesc.text('In order to proceed u need to select product type!').removeClass('text-primary').addClass('text-danger');
        }
        
    }
});

