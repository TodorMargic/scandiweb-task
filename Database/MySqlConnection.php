<?php
namespace Database;

class MysqlConnection 
{
    protected $host;
    protected $dbName;
    protected $username;
    protected $password;

    public function __construct($host, $dbName, $username, $password)
    {
        $this->host = $host;
        $this->dbName = $dbName;
        $this->username = $username;
        $this->password = $password;
    }

    public function connect(): \PDO
    {
        try {
            return new \PDO(
                "mysql:host={$this->host};dbname={$this->dbName};charset=utf8",
                $this->username,
                $this->password
            );
        } catch (\PDOException $e) {
            die($e->getMessage());
        }
    }
}

$connection = new MysqlConnection('localhost', 'scandiweb', 'root','');
$pdo = $connection->connect();
